# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
set(superlu_source  "@superlu_source@")
SET(CFS_SOURCE_DIR "@CFS_SOURCE_DIR@")

#=============================================================================
# Include some convenient macros (e.g. for applying patches).
#=============================================================================
INCLUDE("${CFS_SOURCE_DIR}/cmake_modules/CFS_macros.cmake")

#=============================================================================
# Copy some sources over to source directory.
#=============================================================================
EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/CMakeLists.txt"
  "${superlu_source}/CMakeLists.txt"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/CMake/ResolveCompilerPaths.cmake"
  "${superlu_source}/CMake/ResolveCompilerPaths.cmake"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/example/CMakeLists.txt"
  "${superlu_source}/EXAMPLE/CMakeLists.txt"
)

# For GNU getopt replacements for MSVC cf.
# http://www.codeproject.com/Articles/1940/XGetopt-A-Unix-compatible-getopt-for-MFC-and-Win32
# http://www.codeproject.com/Articles/157001/Full-getopt-Port-for-Unicode-and-Multibyte-Microso
# http://plexfx.org/news/files/70411c2c371bd686273a8bebc42c45c5-3.html
EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/example/xgetopt.h"
  "${superlu_source}/EXAMPLE/xgetopt.h"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/example/xgetopt.c"
  "${superlu_source}/EXAMPLE/xgetopt.c"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/src/CMakeLists.txt"
  "${superlu_source}/SRC/CMakeLists.txt"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/testing/CMakeLists.txt"
  "${superlu_source}/TESTING/CMakeLists.txt"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/testing/stest.cmake.in"
  "${superlu_source}/TESTING/stest.cmake.in"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/testing/ctest.cmake.in"
  "${superlu_source}/TESTING/ctest.cmake.in"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/testing/dtest.cmake.in"
  "${superlu_source}/TESTING/dtest.cmake.in"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/testing/ztest.cmake.in"
  "${superlu_source}/TESTING/ztest.cmake.in"
)

EXECUTE_PROCESS(
  COMMAND ${CMAKE_COMMAND}
  -E copy_if_different
  "${CFS_SOURCE_DIR}/cfsdeps/superlu/testing/matgen/CMakeLists.txt"
  "${superlu_source}/TESTING/MATGEN/CMakeLists.txt"
)

#=============================================================================
# Apply some patches.
#=============================================================================
SET(patches
  # The MSVC runtime complained about uninitialized variables.
  "superlu-matgen.patch"
)
APPLY_PATCHES("${patches}" "${CFS_SOURCE_DIR}/cfsdeps/superlu")
