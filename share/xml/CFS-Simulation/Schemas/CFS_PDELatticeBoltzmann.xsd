<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	targetNamespace="http://www.cfs++.org/simulation"
    xmlns="http://www.cfs++.org/simulation"
	xmlns:cfs="http://www.cfs++.org/simulation"
    elementFormDefault="qualified">

	<xsd:annotation>
		<xsd:documentation xml:lang="en">
			Coupled Field Solver
			project CFS++
			Schema for PDE description for an extern
			Lattice-Boltzmann PDE
		</xsd:documentation>
	</xsd:annotation>


	<!-- ******************************************************************* -->
	<!-- Definition of element for LatticeBoltzmann PDEs -->
	<!-- ******************************************************************* -->
	<xsd:element name="LatticeBoltzmann" type="DT_PDEExternLBM"
		substitutionGroup="PDEBasic">
		<xsd:unique name="CS_ExternLBMRegion">
			<xsd:selector xpath="cfs:region" />
			<xsd:field xpath="@name" />
		</xsd:unique>
	</xsd:element>

	<!-- ******************************************************************* -->
	<!-- Definition of data type for LatticeBoltzmann PDEs -->
	<!-- ******************************************************************* -->

	<xsd:complexType name="DT_PDEExternLBM">
		<xsd:complexContent>
			<xsd:extension base="DT_PDEBasic">
				<xsd:sequence>

					<!-- Regions the PDE lives on -->
					<xsd:element name="regionList" minOccurs="1" maxOccurs="1">
						<xsd:complexType>
							<xsd:sequence>
								<xsd:element name="region" minOccurs="1" maxOccurs="unbounded">
									<xsd:complexType>
										<xsd:attribute name="name" type="xsd:token" use="required" />
                    <xsd:attribute name="boundary" type="xsd:boolean" use="required" />
										<xsd:attribute name="nonLinIds" type="xsd:token" use="optional" default=""/>
									</xsd:complexType>
								</xsd:element>
							</xsd:sequence>
						</xsd:complexType>
					</xsd:element>

					<!-- Boundary Conditions & Loads (optional) -->
					<xsd:element name="bcsAndLoads" minOccurs="0" maxOccurs="unbounded">
						<xsd:complexType>
							<xsd:choice maxOccurs="unbounded">
								<xsd:element name="inlet" minOccurs="0" maxOccurs="3">
									<xsd:complexType>
										<xsd:attribute name="name" type="xsd:token" use="required" />
										<xsd:attribute name="dof" type="DT_ExternLBMDOF" use="required" />
										<xsd:attribute name="value" type="xsd:double" use="required" />
									</xsd:complexType>
								</xsd:element>
								<xsd:element name="outlet" minOccurs="0" maxOccurs="3">
									<xsd:complexType>
										<xsd:attribute name="name" type="xsd:token" use="required" />
										<xsd:attribute name="dof" type="DT_ExternLBMDOF" use="optional" />
										<xsd:attribute name="value" type="xsd:double"	use="optional" />
									</xsd:complexType>
								</xsd:element>
							</xsd:choice>
						</xsd:complexType>
					</xsd:element>

					<!-- Configurations for LBM simulation -->
					<xsd:element name="LBM" minOccurs="1" maxOccurs="1">
						<xsd:complexType>

						<xsd:attribute name="solveAdjoint" type="DT_LBMAdjoint" use="optional" default="external">
							<xsd:annotation>
								<xsd:documentation>Adjoint system can be solved externally via e.g.
									PARDISO or internally with an adjoint LBM formulation
								</xsd:documentation>
							</xsd:annotation>
						</xsd:attribute>
            
            <xsd:attribute name="omega" type="xsd:double" use="optional"> 
              <xsd:annotation> 
                <xsd:documentation>Tuning parameter for LBM (SRT and MRT). small is more robust, large faster \n 
                  If omega and Re are set, u_x inflow is adjusted \n If Re and u_x are set, omega is adjusted \n If omega and u_x are set, Re is printed out in .info.xml file 
                </xsd:documentation> 
              </xsd:annotation> 
            </xsd:attribute> 
             
            <xsd:attribute name="Re" type="xsd:double" use="optional"> 
              <xsd:annotation> 
                <xsd:documentation> Dimensionless characteristic of flow. ratio of inertial forces to viscous forces; at least omega or Re must be specified</xsd:documentation> 
              </xsd:annotation> 
            </xsd:attribute> 
             
            <xsd:attribute name="inflowProfile" type="DT_ExternLBMInflow" use="optional" default="uniform"> 
              <xsd:annotation> 
                <xsd:documentation>Specify inflow velocity profile. \n 
                  For parabolic profile, velocity specified in "bcAndLoads" is assumed as maximum velocity. So far, only a parabolic velocity profile in x-direction is possible, 
                </xsd:documentation> 
                </xsd:annotation> 
              </xsd:attribute> 					
              
							<xsd:attribute name="maxWallTime" type="xsd:double" use="required" />
              
							<xsd:attribute name="maxIter" type="xsd:float" use="optional" default="1e6" >
                <xsd:annotation>
                  <xsd:documentation>upper bound of the LBM iterations</xsd:documentation>
                </xsd:annotation>
              </xsd:attribute>

  					 <xsd:attribute name="convergence" type="xsd:double" use="required">
              <xsd:annotation>
                <xsd:documentation>convergence tolerance for the residuum of the LBM solver</xsd:documentation>
               </xsd:annotation>              
             </xsd:attribute>
             
             <xsd:attribute name="writeFrequency" type="xsd:positiveInteger" use="optional" default="999999"> 
               <xsd:annotation> 
                  <xsd:documentation>frequency for writing out simulation results, e.g. for 10: every 10th iteration results are written out</xsd:documentation> 
               </xsd:annotation>
              </xsd:attribute> 
                   
             <xsd:attribute name="solver" use="optional" default="internal">
                <xsd:annotation>
                  <xsd:documentation>which LBM solver to use. 'internal' by default.</xsd:documentation>
                </xsd:annotation>                 
								<xsd:simpleType>
	                <xsd:restriction base="xsd:token">
                    <xsd:enumeration value="internal">  
                      <xsd:annotation> 
                        <xsd:documentation> Use internal solver.  </xsd:documentation> 
                      </xsd:annotation> 
                    </xsd:enumeration> 
                    <!-- any external solver -->
                    <xsd:enumeration value="external"> 
                    <xsd:annotation> 
                      <xsd:documentation> 
                        Use external solver. Output is file 'CFS2LBM.dat' with information on LBM parameters; Input file is file from external solver with  
                        information on all PDFS(D2Q9 or D3Q19 model). 
                      </xsd:documentation> 
                    </xsd:annotation> 
                  </xsd:enumeration> 
	                </xsd:restriction>
	              </xsd:simpleType>
              </xsd:attribute>
  
              <xsd:attribute name="lbm" type="xsd:token" use="optional" default="./" >
                <xsd:annotation>
                  <xsd:documentation>if solver is not 'internal' give here the execuable</xsd:documentation>
                </xsd:annotation>
              </xsd:attribute>              
              
              <xsd:attribute name="plot" type="xsd:boolean" use="optional" default="false" >
                <xsd:annotation>
                  <xsd:documentation>creates for the internal LBM solver simulation.lbm.dat with residuum over LBM iteration.</xsd:documentation>
                  </xsd:annotation>
              </xsd:attribute>
              
            </xsd:complexType>
					</xsd:element>

					<!-- Desired solution values (optional) -->
					<xsd:element name="storeResults" minOccurs="0" maxOccurs="1">
						<xsd:complexType>
						  <xsd:sequence>
						  
							  <!-- Element result definition -->
				        <xsd:element name="elemResult" minOccurs="0" maxOccurs="unbounded">
				          <xsd:complexType>
				            <xsd:complexContent>
				              <xsd:extension base="DT_ElemResult">
				                <xsd:attribute name="type" type="DT_ExternLBMElemResult" use="required"/>
				              </xsd:extension>
				            </xsd:complexContent>
				          </xsd:complexType>
				        </xsd:element>
                
                <!-- Element result definition -->
                <xsd:element name="nodeResult" minOccurs="0" maxOccurs="unbounded">
                  <xsd:complexType>
                    <xsd:complexContent>
                      <xsd:extension base="DT_ElemResult">
                        <xsd:attribute name="type" type="DT_ExternLBMNodeResult" use="required"/>
                      </xsd:extension>
                    </xsd:complexContent>
                  </xsd:complexType>
                </xsd:element>
						  
						  </xsd:sequence>
						</xsd:complexType>
					</xsd:element>

				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<!-- ******************************************************************* -->
	<!-- Definition of enumeration type describing the degrees of freedom -->
	<!-- ******************************************************************* -->

	<xsd:simpleType name="DT_ExternLBMDOF">
		<xsd:restriction base="xsd:token">
			<xsd:enumeration value="x" />
			<xsd:enumeration value="y" />
			<xsd:enumeration value="z" />
		</xsd:restriction>
	</xsd:simpleType>

	<!-- ******************************************************************* -->
	<!-- Definition of syntax for specifying output quantity of CFS -->
	<!-- ******************************************************************* -->
	<xsd:simpleType name="DT_ExternLBMElemResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="LBMVelocity"/>
      <xsd:enumeration value="LBMAdjointVelocity"/>
      <xsd:enumeration value="LBMProbabilityDistribution"/>
      <xsd:enumeration value="LBMProbabilityDistributionAdjoint"/> 
      <xsd:enumeration value="LBMDensity"/>
      <xsd:enumeration value="LBMPressure"/>      
      <xsd:enumeration value="mechPseudoDensity"/>
      <xsd:enumeration value="LBMPhysicalPseudoDensity"/>
      <xsd:enumeration value="optResult_1"/>
      <xsd:enumeration value="optResult_2"/>
      <xsd:enumeration value="optResult_3"/>
      <xsd:enumeration value="optResult_4"/>
      <xsd:enumeration value="optResult_5"/>
    </xsd:restriction>
  </xsd:simpleType>
  
  <xsd:simpleType name="DT_ExternLBMNodeResult">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="optResult_1"/>
      <xsd:enumeration value="optResult_2"/>
      <xsd:enumeration value="optResult_3"/>
      <xsd:enumeration value="optResult_4"/>
      <xsd:enumeration value="optResult_5"/>
    </xsd:restriction>
  </xsd:simpleType>
  
  <!-- ******************************************************************* --> 
  <!-- Definition of inflow profiles --> 
  <!-- ******************************************************************* --> 
  <xsd:simpleType name="DT_ExternLBMInflow"> 
    <xsd:restriction base="xsd:token"> 
      <xsd:enumeration value="uniform" /> 
      <xsd:enumeration value="parabolic" /> 
    </xsd:restriction> 
  </xsd:simpleType>
  
  <!-- ******************************************************************* --> 
  <!-- Definition of relaxation models profiles --> 
  <!-- ******************************************************************* --> 
  <xsd:simpleType name="DT_LBMAdjoint"> 
    <xsd:restriction base="xsd:token"> 
      <xsd:enumeration value="internal"/> 
      <xsd:enumeration value="external"/> 
    </xsd:restriction> 
  </xsd:simpleType>
</xsd:schema>

