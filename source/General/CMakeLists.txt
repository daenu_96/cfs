SET(GENERAL_SRCS Environment.cc Exception.cc)

ADD_LIBRARY(cfsgeneral STATIC ${GENERAL_SRCS})

ADD_DEPENDENCIES(cfsgeneral boost)
