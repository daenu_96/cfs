The .gitlab directory
=====================

This directory is read by gitlab to provide several features or customisations.

* [Description templates](https://docs.gitlab.com/ee/user/project/description_templates.html) for issues and merge requests
* define [code owners](https://docs.gitlab.com/ee/user/project/code_owners.html) for extended push/review rules
* for an extensive example see: https://gitlab.com/gitlab-org/gitlab/-/tree/master/.gitlab 
